﻿using System;

namespace Solid_NetCore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Cotizador de pólizas de seguro");

            Cotizador cotizador = new Cotizador();
            if (cotizador.CalcularSumaAsegurada() > 0)
            {
                Console.WriteLine("Cotizador de pólizas de seguro");
            }
        }
    }
}
