﻿using System;

namespace Solid
{
    class Program
    {
        /*
         1. SRP Single Responsibility Principle
         2. OCP Open Closed Principle
         3. LSP Liskov Substitution Principle
         4. ISP Interface Segretation Principle
         5. Dependecy Inversion Principle
         */
        static void Main(string[] args)
        {
            Console.WriteLine("Cotizador de pólizas de seguro");

            CotizadorService cotizador = new CotizadorService();
            
            cotizador.Cotizar();

            if (cotizador.MontoCoberturaPoliza > 0)
            {
                Console.WriteLine(string.Format($"Suma asegurada: {0}", cotizador.MontoCoberturaPoliza));
            }
            else
            {
                Console.WriteLine("No se pudo generar la suma asegurada");
            }
        }
    }
}
