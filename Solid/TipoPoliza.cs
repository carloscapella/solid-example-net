﻿namespace Solid
{
    public class TipoPoliza
    {
        public string Nombre { get; set; }
        public string IdProducto { get; set; }
        public string Plan { get; set; }
    }
}
