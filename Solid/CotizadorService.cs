﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.IO;

namespace Solid
{
    internal class CotizadorService
    {
        public double MontoCoberturaPoliza { get; internal set; }

        internal void Cotizar()
        {
            Console.WriteLine("Cotizando póliza");

            string json = File.ReadAllText("DatosCliente.json");

            var policy = JsonConvert.DeserializeObject<PolizaModel>(json, new StringEnumConverter());

            switch (policy.TipoPoliza.IdProducto)
            {
                case "02B":
                    Console.WriteLine("Cotizando póliza de auto");
                    if (policy.Kilometraje > 30000)
                    {
                        MontoCoberturaPoliza = policy.PrecioAutomovil * 0.45;
                    }
                    else
                    {
                        MontoCoberturaPoliza = policy.PrecioAutomovil * 0.8;
                    }

                    break;

                case "02BA":
                    Console.WriteLine("Cotizando póliza de vida");
                    if (policy.PresentaAntecedentes)
                    {
                        MontoCoberturaPoliza = policy.SumaAsegurada * 0.7;
                    }
                    else
                    {
                        MontoCoberturaPoliza = policy.SumaAsegurada;
                    }

                    break;

                case "02BAa":
                    Console.WriteLine("Cotizando póliza de incendio");
                    if (policy.AniosAntiguedad > 5)
                    {
                        MontoCoberturaPoliza = policy.Evaluo * 0.30;
                    }
                    else
                    {
                        MontoCoberturaPoliza = policy.Evaluo * 0.90;
                    }
                    break;
                default:
                    Console.WriteLine("Póliza no válida");
                    break;
            }
        }
    }
}