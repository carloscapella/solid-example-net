﻿namespace Solid
{
    public class PolizaModel
    {
        public TipoPoliza TipoPoliza { get; set; }

        // Seguro de Auto
        public string Modelo { get; set; }
        public int Kilometraje { get; set; }
        public double PrecioAutomovil { get; set; }

        // seguro de vida
        public string NombreAsegurado { get; set; }
        public double SumaAsegurada{ get; set; }
        public bool PresentaAntecedentes { get; set; }

        // Seguro de Incendio
        public double Evaluo { get; set; }
        public int AniosAntiguedad { get; set; }


    }
}