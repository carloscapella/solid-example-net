﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Solid;
using System.IO;
using Xunit;

namespace SolidTest
{
    public class CotizadorTests
    {
        [Fact]
        public void ConvertPolicyFromJson()
        {
            string json = File.ReadAllText(@"../../../DatosCliente.json");

            var policy = JsonConvert.DeserializeObject<PolizaModel>(json, new StringEnumConverter());

            Assert.NotNull(policy);
        }
    }
}
